
//typedef  uint8_t;
#define ROTL8(x,shift) ((uint8_t) ((x) << (shift)) | ((x) >> (8 - (shift))))
//#include "AES.h"
#include "message.h"

//Landon Taylor
//9-22-15
//Network Security
//Prof. Singhal

//Advanced Encryption System
//AKA Rijndael

void AES(Message &m, uint8_t *expandedKey) {

	uint8_t state[4*numCols];
	uint8_t r, i, j;

	for (i = 0; i < 4; i++) {
		for (j = 0; j < numCols; j++) {
			state[numCols*i+j] = m.arr[i+4*j];
		}
	}

	add_round_key(state, expandedKey, 0);

	for (r = 1; r < numberOfRounds; r++) {
		sub_bytes(state);
		shift_rows(state);
		mix_columns(state);
		add_round_key(state, expandedKey, r);
	}

	sub_bytes(state);
	shift_rows(state);
	add_round_key(state, expandedKey, numberOfRounds);

	for (i = 0; i < 4; i++) {
		for (j = 0; j < numCols; j++) {
			m.arr[i+4*j] = state[numCols*i+j];
		}
	}
}


void AESD(Message &m, uint8_t *roundKey)
{
	uint8_t state[4*numCols];
	uint8_t r, i, j;

	for (i = 0; i < 4; i++) {
		for (j = 0; j < numCols; j++) {
			state[numCols*i+j] = m.arr[i+4*j];
		}
	}

	add_round_key(state, roundKey, numberOfRounds);

	for (r = numberOfRounds-1; r >= 1; r--) {
		inv_shift_rows(state);
		inv_sub_bytes(state);
		add_round_key(state, roundKey, r);
		inv_mix_columns(state);
	}

	inv_shift_rows(state);
	inv_sub_bytes(state);
	add_round_key(state, roundKey, 0);

	for (i = 0; i < 4; i++) {
		for (j = 0; j < numCols; j++) {
			m.arr[i+4*j] = state[numCols*i+j];
		}
	}
}

int main()
{
/*
    uint8_t key[] = {
		0x00, 0x01, 0x02, 0x03,
		0x04, 0x05, 0x06, 0x07,
		0x08, 0x09, 0x0a, 0x0b,
		0x0c, 0x0d, 0x0e, 0x0f};
*/
/*
    uint8_t key[] = {
    0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c} ;
    */
/*
    uint8_t key[] = {
        0xff,0xff,0xff,0xff,
        0xff,0xff,0xff,0xff,
        0xff,0xff,0xff,0xff,
        0xff,0xff,0xff,0xff};
*/
    uint8_t key[]={0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f};
    //char mystr[] = {0x66,'h','i','m','y','n','a','m','e',' ','i','s','w','h','a','t'};
    char mystr[] = {0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff} ;

    Message myM(mystr);

    uint8_t *expandedKey; // expanded key
    switch (sizeof(key)) {
		default:
		case 16: keyRows = 4;
                 numberOfRounds = 10;
                 break;
	}

	expandedKey = (uint8_t*)malloc(numCols*(numberOfRounds+1)*4);

	key_expansion(key, expandedKey);

/*
    cout << "expanded key is: \n";
    for(int i=0; i<176; i++)
    {
        cout << std::hex << (int)w[i] << ", ";// << std::dec ;

        if(i!=0 && i%15 == 0)
        {
            cout << endl;
        }
    }

    cout << std::dec <<endl <<endl;
*/
    //myM.printStr();
     cout <<"my string in hex (FIPS C.1 example): " <<endl;
	for( int i =0; i<16; i++)
        cout << std::hex << (int) myM.arr[i] << ", ";
    cout <<endl;
	cout << "starting cypher..." << endl;
	AES(myM, expandedKey);
	cout << "cypher done..." << endl;

    //myM.printStr();
    cout << endl;
    cout <<"my string in hex (FIPS): " <<endl;
	for( int i =0; i<16; i++)
        cout << std::hex << (int) myM.arr[i] << ", ";
    cout <<endl;

    AESD(myM, expandedKey);
	cout << "\n decypher done...\n";

	//myM.printStr();

	cout << endl;
    cout <<"my string in hex (FIPS): " <<endl;
	for( int i =0; i<16; i++)
        cout << std::hex << (int) myM.arr[i] << ", ";
    cout <<endl;


    cout <<endl;
    cout << "message one done, trying second message..." << endl;
	char mystr2[] = {0x66,0x75,0x75,'a','h','a','h','a','h','a','h','a','x','j','a','q'};
	Message myM2(mystr2);
    myM2.printStr();
    AES(myM2, expandedKey);
    myM2.printStr();
    AESD(myM2,expandedKey);
    myM2.printStr();
}
