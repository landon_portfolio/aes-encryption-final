#ifndef MESSAGE_H
#define MESSAGE_H

#include "AES.h"

class Message
{
    public:
        uint8_t arr[16];
        Message(){}
        Message(char c[]){
            for(int i=0;i<16;i++)
            {
                arr[i]=c[i];
            }
        }
        //virtual
        ~Message(){}

        void printStr()
        {
            std:cout << "printing..." << std::endl;
            for(int i=0;i<16;i++)
            {
                std::cout << (char) arr[i];
            }
            std::cout << std::endl;
        }

    protected:
    private:
};

#endif // MESSAGE_H
